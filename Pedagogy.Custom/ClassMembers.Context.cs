﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Pedagogy.Custom
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class ClassMembers : DbContext
    {
        public ClassMembers()
            : base("name=ClassMembers")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<CertificateType> CertificateTypes { get; set; }
        public virtual DbSet<ClassCertificate> ClassCertificates { get; set; }
        public virtual DbSet<ClassEvaluation> ClassEvaluations { get; set; }
        public virtual DbSet<ClassReviewQuestionDetail> ClassReviewQuestionDetails { get; set; }
        public virtual DbSet<ClassTestDetail> ClassTestDetails { get; set; }
        public virtual DbSet<ClassWorkflow> ClassWorkflows { get; set; }
        public virtual DbSet<Contact> Contacts { get; set; }
        public virtual DbSet<ContactFacility> ContactFacilities { get; set; }
        public virtual DbSet<ContactType> ContactTypes { get; set; }
        public virtual DbSet<EducationPlan> EducationPlans { get; set; }
        public virtual DbSet<EducationPlanClass> EducationPlanClasses { get; set; }
        public virtual DbSet<EducationPlanLicense> EducationPlanLicenses { get; set; }
        public virtual DbSet<EventLog> EventLogs { get; set; }
        public virtual DbSet<EventLogType> EventLogTypes { get; set; }
        public virtual DbSet<FacilityClass> FacilityClasses { get; set; }
        public virtual DbSet<FacilityMembership> FacilityMemberships { get; set; }
        public virtual DbSet<FacilityPreference> FacilityPreferences { get; set; }
        public virtual DbSet<LicenseCategory> LicenseCategories { get; set; }
        public virtual DbSet<LicenseType> LicenseTypes { get; set; }
        public virtual DbSet<SpeechRate> SpeechRates { get; set; }
        public virtual DbSet<StudentClass> StudentClasses { get; set; }
        public virtual DbSet<StudentClassDetail> StudentClassDetails { get; set; }
        public virtual DbSet<StudentLicens> StudentLicenses { get; set; }
        public virtual DbSet<StudentMembership> StudentMemberships { get; set; }
        public virtual DbSet<StudentTestAttempt> StudentTestAttempts { get; set; }
        public virtual DbSet<Facility> Facilities { get; set; }
    }
}
