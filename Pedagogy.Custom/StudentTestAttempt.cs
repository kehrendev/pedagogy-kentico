//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Pedagogy.Custom
{
    using System;
    using System.Collections.Generic;
    
    public partial class StudentTestAttempt
    {
        public int TestAttemptID { get; set; }
        public int StudentClassID { get; set; }
        public bool TestComplete { get; set; }
        public Nullable<System.DateTime> TestCompletedOn { get; set; }
        public Nullable<decimal> TestScore { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
    }
}
