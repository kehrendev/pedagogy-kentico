//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Pedagogy.Custom
{
    using System;
    using System.Collections.Generic;
    
    public partial class EventLog
    {
        public int EventLogID { get; set; }
        public string UsersName { get; set; }
        public string UsersUserName { get; set; }
        public string StudentName { get; set; }
        public string StudentUserName { get; set; }
        public string CourseName { get; set; }
        public int EventLogTypeID { get; set; }
        public System.DateTime EventCreatedOn { get; set; }
    
        public virtual EventLogType EventLogType { get; set; }
    }
}
