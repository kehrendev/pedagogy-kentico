﻿using CMS;
using CMS.CustomTables;
using CMS.DataEngine;
using CMS.DocumentEngine;
using CMS.EmailEngine;
using CMS.Helpers;
using CMS.Localization;

using Pedagogy.Custom.CustomCourseUpdateModule;

using System.Linq;

[assembly: RegisterModule(typeof(CustomCourseUpdateModule))]
namespace Pedagogy.Custom.CustomCourseUpdateModule
{
    public class CustomCourseUpdateModule : Module
    {
        public CustomCourseUpdateModule() : base("CustomCourseUpdateModuleInit")
        {
        }

        protected override void OnInit()
        {
            base.OnInit();

            DocumentEvents.Update.Before += Course_Updated;
        }

        private void Course_Updated(object sender, DocumentEventArgs e)
        {
            var page = e.Node;

            if (page.ClassName == "Pedagogy.Course" || page.ClassName == "Pedagogy.CourseMembership")
            {
                var p = DocumentHelper.GetDocuments()
                    .Type(page.ClassName)
                    .WhereEquals(nameof(TreeNode.NodeGUID), page.NodeGUID)
                    .FirstOrDefault();

                if (p != null)
                {
                    if (p.GetBooleanValue("CoursePriceTBD", false))
                    {
                        var items = CustomTableItemProvider.GetItems("Pedagogy.CourseWaitList")
                            .WhereStartsWith("CourseGUID", p.NodeGUID.ToString())
                            .ToList();

                        foreach (var item in items)
                        {
                            var customerEmail = item.GetStringValue("CustomerEmail", "");
                            var email = new EmailMessage
                            {
                                Recipients = !string.IsNullOrEmpty(customerEmail) ? customerEmail : "",
                                Subject = ResHelper.GetString("course.wait.list.email.subject", LocalizationContext.CurrentCulture.CultureCode),
                                Body = $"{page.GetStringValue("CourseTitle", "")} is now available for purchase. Click <a href='dev.pedagogyeducation.com{p.NodeAliasPath}'>here</a> to add this course to your cart.",
                                From = ResHelper.GetString("site.no.reply.email", LocalizationContext.CurrentCulture.CultureCode)
                            };

                            EmailSender.SendEmail(email);

                            item.Delete();
                        }
                    }
                }
            }
        }
    }
}
