﻿using CMS;
using CMS.DataEngine;
using CMS.Ecommerce;
using CMS.SiteProvider;

using Pedagogy.Custom.CustomOrderStatusUpdateModule;

using System;
using System.Collections.Generic;
using System.Linq;

[assembly: RegisterModule(typeof(CustomOrderStatusUpdateModule))]
namespace Pedagogy.Custom.CustomOrderStatusUpdateModule
{
    public class CustomOrderStatusUpdateModule : Module
    {
        private ClassMembers db = new ClassMembers();

        // Module class constructor, the system registers the module under the name "CustomInit"
        public CustomOrderStatusUpdateModule()
            : base("CustomInit")
        {
        }

        // Contains initialization code that is executed when the application starts
        protected override void OnInit()
        {
            base.OnInit();

            // Assigns custom handlers to events
            EcommerceEvents.OrderPaid.Execute += Order_Paid;
        }

        private void Order_Paid(object sender, OrderPaidEventArgs e)
        {
            OrderInfo order = e.Order;

            var dropped = OrderStatusInfoProvider.GetOrderStatusInfo("coursesDropped", SiteContext.CurrentSiteName).StatusID;
            var paid = OrderStatusInfoProvider.GetOrderStatusInfo("paid", SiteContext.CurrentSiteName);
            var history = OrderStatusUserInfoProvider.GetOrderStatusHistory(order.OrderID).Reverse().ToList();
            bool coursesAlreadyAdded = false;
            foreach (var item in history)
            {
                if (item.ToStatusID == paid.StatusID) coursesAlreadyAdded = true;
            }

            if (order.OrderStatusID == dropped && !coursesAlreadyAdded)
            {
                if (order.OrderCustomData.ContainsColumn("FacilityID"))
                {
                    var items = ShoppingCartInfoProvider.GetShoppingCartInfoFromOrder(order.OrderID).CartItems;
                    var facilityID = int.Parse(order.OrderCustomData.GetValue("FacilityID").ToString());

                    //Facility membership purchase
                    if (order.OrderCustomData.ContainsColumn("MembershipPurchase"))
                    {
                        var amount = int.Parse(order.OrderCustomData.GetValue("MembershipCount").ToString());
                        var today = DateTime.Today;
                        var expDate = today.AddYears(1).AddDays(1).AddSeconds(-1);

                        for (var i = 1; i <= amount; i++)
                        {
                            var memName = order.OrderCustomData.GetValue($"Membership-{i}-Name").ToString();
                            var qty = order.OrderCustomData.GetValue($"Membership-{i}-Qty").ToString();

                            var newMem = new FacilityMembership
                            {
                                FacilityID = facilityID,
                                MembershipName = memName,
                                ExpirationDate = expDate
                            };

                            db.FacilityMemberships.Add(newMem);
                            db.SaveChanges();

                            var mem = db.FacilityMemberships
                                .Where(m => m.FacilityID == facilityID)
                                .Where(m => m.MembershipName.Contains(memName))
                                .Where(m => m.ExpirationDate == expDate)
                                .FirstOrDefault();

                            var classIDs = order.OrderCustomData.GetValue($"Membership-{i}-ClassIDs").ToString();
                            var ids = classIDs.Split(',');

                            foreach (var item in ids)
                            {
                                foreach (var cartItem in items)
                                {
                                    if (cartItem.SKU.SKUNumber == item)
                                    {
                                        items.Remove(cartItem);
                                        break;
                                    }
                                }

                                var facilityClass = new FacilityClass
                                {
                                    FacilityID = facilityID,
                                    ClassID = int.Parse(item),
                                    Quantity = int.Parse(qty),
                                    CreatedOn = today,
                                    FacilityMembershipID = mem.FacilityMembershipID,
                                    CanPrint = false,
                                    InvoiceNumber = order.OrderInvoiceNumber
                                };

                                db.FacilityClasses.Add(facilityClass);
                                db.SaveChanges();
                            }
                        }
                    }
                    //Facility regular course purchase
                    else
                    {
                        foreach (var x in items)
                        {
                            if (!x.IsProductOption)
                            {
                                var newFacilityClass = new FacilityClass
                                {
                                    FacilityID = facilityID,
                                    Quantity = x.CartItemUnits,
                                    CreatedOn = DateTime.Now,
                                    InvoiceNumber = order.OrderInvoiceNumber,
                                    CanPrint = CanPrintClass(items, x)
                                };

                                if (x.SKU.SKUProductType == SKUProductTypeEnum.Product && !x.IsBundleItem)
                                {
                                    newFacilityClass.ClassID = int.Parse(x.SKU.SKUNumber);

                                    db.FacilityClasses.Add(newFacilityClass);
                                    db.SaveChanges();
                                }
                                else if (x.SKU.SKUProductType == SKUProductTypeEnum.Bundle)
                                {
                                    var bundleItems = SKUInfoProvider.GetBundleItems(x.SKUID);

                                    foreach (var i in bundleItems)
                                    {
                                        if (!i.IsProductOption)
                                        {
                                            newFacilityClass.ClassID = int.Parse(i.SKUNumber);

                                            db.FacilityClasses.Add(newFacilityClass);
                                            db.SaveChanges();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                order.OrderIsPaid = false;
                OrderInfoProvider.SetOrderInfo(order);
            }
        }

        private bool CanPrintClass(IEnumerable<ShoppingCartItemInfo> cartItems, ShoppingCartItemInfo currentItem)
        {
            foreach (var y in cartItems)
            {
                if (y.IsProductOption && y.ParentProduct.SKUID == currentItem.SKUID && y.TotalPrice != 0)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
