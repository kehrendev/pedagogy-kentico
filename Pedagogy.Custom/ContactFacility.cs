//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Pedagogy.Custom
{
    using System;
    using System.Collections.Generic;
    
    public partial class ContactFacility
    {
        public int ContactFacilityID { get; set; }
        public Nullable<int> ContactID { get; set; }
        public Nullable<int> FacilityID { get; set; }
        public Nullable<bool> Enabled { get; set; }
        public Nullable<int> OldFacilityID { get; set; }
        public Nullable<int> OldCorporationID { get; set; }
        public Nullable<int> OldContactID { get; set; }
        public Nullable<bool> IsFacilityAdmin { get; set; }
        public Nullable<bool> IsCorpAdmin { get; set; }
    }
}
