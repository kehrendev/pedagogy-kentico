//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Pedagogy.Custom
{
    using System;
    using System.Collections.Generic;
    
    public partial class Facility
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Facility()
        {
            this.Facility1 = new HashSet<Facility>();
        }
    
        public int FacilityID { get; set; }
        public string Name { get; set; }
        public string FacilityCode { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public bool Enabled { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string FacilitySource { get; set; }
        public int PartnerCode { get; set; }
        public Nullable<System.DateTime> LastReportDate { get; set; }
        public string AcctingCode { get; set; }
        public Nullable<int> FacilityParentID { get; set; }
        public Nullable<bool> FacilityIsCorporation { get; set; }
        public Nullable<int> FacilityPreferenceID { get; set; }
        public string PrimaryName { get; set; }
        public string PrimaryEmail { get; set; }
        public string PrimaryPhone { get; set; }
        public string AlternateName { get; set; }
        public string AlternateEmail { get; set; }
        public string AlternatePhone { get; set; }
        public string APName { get; set; }
        public string APEmail { get; set; }
        public string APPhone { get; set; }
        public Nullable<int> OldCorpID { get; set; }
        public string AdministratorName { get; set; }
        public string AdministratorEmail { get; set; }
        public string AdministratorPhone { get; set; }
        public string DONName { get; set; }
        public string DONEmail { get; set; }
        public string DONPhone { get; set; }
        public string EducatorName { get; set; }
        public string EducatorEmail { get; set; }
        public string EducatorPhone { get; set; }
        public Nullable<int> OldFacilityID { get; set; }
        public Nullable<int> OldParentCorpID { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Facility> Facility1 { get; set; }
        public virtual Facility Facility2 { get; set; }
    }
}
