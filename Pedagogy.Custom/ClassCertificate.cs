//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Pedagogy.Custom
{
    using System;
    using System.Collections.Generic;
    
    public partial class ClassCertificate
    {
        public int CertificateID { get; set; }
        public int StudentClassID { get; set; }
        public string ClassName { get; set; }
        public int StudentLicenseTypeID { get; set; }
        public decimal TestScore { get; set; }
        public string CertificateName { get; set; }
        public string CertificateCredits { get; set; }
        public string StateLicense { get; set; }
        public System.DateTime CertificateDate { get; set; }
        public Nullable<decimal> ContactHours { get; set; }
        public Nullable<decimal> CEU { get; set; }
        public int CertificateTypeID { get; set; }
        public Nullable<System.DateTime> PrintedOn { get; set; }
    
        public virtual CertificateType CertificateType { get; set; }
    }
}
