﻿using System;
using System.Collections.Generic;
using CMS;
using CMS.DataEngine;
using CMS.Helpers;
using CMS.MacroEngine;
using Pedagogy.Custom.MacroMethods;

[assembly: RegisterExtension(typeof(CustomMacroMethods), typeof(string))]
[assembly: RegisterExtension(typeof(CustomMacroMethods), typeof(StringNamespace))]
namespace Pedagogy.Custom.MacroMethods
{
    public class CustomMacroMethods : MacroMethodContainer
    {
        [MacroMethod(typeof(List<string>), "Returns a list of fields for the class.", 1)]
        [MacroMethodParam(0, "tableCodeName", typeof(string), "Table code name.")]
        public static object GetTableFields(EvaluationContext context, params object[] parameters)
        {
            {
                switch(parameters.Length)
                {
                    case 1:
                        List<string> lstFields = ClassStructureInfo.GetColumns(ValidationHelper.GetString(parameters[0], ""));
                        return lstFields;
                    default:
                        throw new NotSupportedException();
                }
            }
        }
    }
}
