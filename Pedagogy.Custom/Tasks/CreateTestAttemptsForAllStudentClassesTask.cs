﻿using CMS.Scheduler;

using System;
using System.Linq;

namespace Pedagogy.Custom.Tasks
{
    public class CreateTestAttemptsForAllStudentClassesTask : ITask
    {
        private readonly ClassMembers db = new ClassMembers();

        public string Execute(TaskInfo task)
        {
            var studentClasses = db.StudentClasses
                .Where(x => !x.TestAttemptID.HasValue)
                .ToList();

            foreach (var studentClass in studentClasses)
            {
                var today = DateTime.Now;

                var newTestAttempt = new StudentTestAttempt
                {
                    StudentClassID = studentClass.StudentClassID,
                    TestComplete = studentClass.TestComplete,
                    TestCompletedOn = studentClass.TestCompletedOn,
                    TestScore = studentClass.TestScore,
                    CreatedOn = today,
                    UpdatedOn = null
                };

                db.StudentTestAttempts.Add(newTestAttempt);
                db.Entry(newTestAttempt).State = System.Data.Entity.EntityState.Added;
                db.SaveChanges();

                var attemptID = db.StudentTestAttempts
                        .Where(x => x.StudentClassID == newTestAttempt.StudentClassID)
                        .Where(x => x.TestComplete == newTestAttempt.TestComplete)
                        .Where(x => x.TestCompletedOn == newTestAttempt.TestCompletedOn)
                        .Where(x => x.TestScore == newTestAttempt.TestScore)
                        .Where(x => x.CreatedOn == today)
                        .Select(x => x.TestAttemptID)
                        .FirstOrDefault();

                studentClass.TestAttemptID = attemptID;
                db.Entry(studentClass).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

                var classTestDetails = db.ClassTestDetails.Where(x => x.StudentClassID == studentClass.StudentClassID).ToList();
                foreach (var classTestDetail in classTestDetails)
                {
                    classTestDetail.TestAttemptID = attemptID;
                    db.Entry(classTestDetail).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }

            return null;
        }
    }
}
