﻿using CMS.Membership;
using CMS.Scheduler;
using CMS.SiteProvider;

using System.Linq;

namespace Pedagogy.Custom.Tasks
{
    public class UpdateContactUserRoleAndUserSettings : ITask
    {
        private readonly ClassMembers db = new ClassMembers();
        private const string CORP_ADMIN_CODE_NAME = "corporateAdmin";
        private const string FACILITY_ADMIN_CODE_NAME = "facilityAdmin";
        private const string SITE_ADMIN_CODE_NAME = "siteAdmin";
        private const string STUDENT_CODE_NAME = "student";

        public string Execute(TaskInfo task)
        {
            // get all contacts that have a user account
            var contacts = db.Contacts
                .Where(x => x.ContactUserID.HasValue)
                .OrderBy(x => x.ContactUserID)
                .ThenBy(x => x.ContactTypeID)
                .ToList();

            foreach (var i in contacts)
            {
                var user = UserInfoProvider.GetUserInfo(i.ContactUserID.Value);

                if (user != null)
                {
                    // check if the contact has default password
                    if(i.IsDefaultPassword.HasValue && i.IsDefaultPassword.Value)
                    {
                        var col = user.UserSettings.UserCustomData.GetValue("IsDefaultPassword");

                        if (col == null)
                        {
                            // set the is default password custom column
                            user.UserSettings.UserCustomData.SetValue("IsDefaultPassword", true);
                            user.Update();
                        }
                    }

                    // get the contact type
                    var type = db.ContactTypes.Where(x => x.TypeID == i.ContactTypeID.Value).Select(x => x.TypeCodeName).FirstOrDefault();

                    // assign the user the appropriate role
                    switch (type)
                    {
                        case CORP_ADMIN_CODE_NAME:
                            // check to make sure they aren't in a higher role
                            if (user.IsInRole(SITE_ADMIN_CODE_NAME, SiteContext.CurrentSiteName)) break;

                            // check to make sure they don't have a lower priority role
                            if(user.IsInRole(STUDENT_CODE_NAME, SiteContext.CurrentSiteName)) UserInfoProvider.RemoveUserFromRole(user.UserName, STUDENT_CODE_NAME, SiteContext.CurrentSiteName);
                            if (user.IsInRole(FACILITY_ADMIN_CODE_NAME, SiteContext.CurrentSiteName)) UserInfoProvider.RemoveUserFromRole(user.UserName, FACILITY_ADMIN_CODE_NAME, SiteContext.CurrentSiteName); ;

                            // add user to role
                            UserInfoProvider.AddUserToRole(user.UserName, CORP_ADMIN_CODE_NAME, SiteContext.CurrentSiteName);

                            break;

                        case FACILITY_ADMIN_CODE_NAME:
                            // check to make sure they aren't in a higher role
                            if (user.IsInRole(CORP_ADMIN_CODE_NAME, SiteContext.CurrentSiteName) || user.IsInRole(SITE_ADMIN_CODE_NAME, SiteContext.CurrentSiteName)) break;

                            // check to make sure they don't have a lower priority role
                            if (user.IsInRole(STUDENT_CODE_NAME, SiteContext.CurrentSiteName)) UserInfoProvider.RemoveUserFromRole(user.UserName, STUDENT_CODE_NAME, SiteContext.CurrentSiteName);

                            // add user to role
                            UserInfoProvider.AddUserToRole(user.UserName, FACILITY_ADMIN_CODE_NAME, SiteContext.CurrentSiteName);

                            break;

                        default:
                            // check to make sure they aren't in a higher role
                            if (user.IsInRole(CORP_ADMIN_CODE_NAME, SiteContext.CurrentSiteName) || user.IsInRole(SITE_ADMIN_CODE_NAME, SiteContext.CurrentSiteName) 
                                || user.IsInRole(FACILITY_ADMIN_CODE_NAME, SiteContext.CurrentSiteName)) break;

                            // add user to role
                            UserInfoProvider.AddUserToRole(user.UserName, STUDENT_CODE_NAME, SiteContext.CurrentSiteName);

                            break;
                    }
                }
            }

            return null;
        }
    }
}