﻿using CMS.DataEngine;
using CMS.Scheduler;
using System;
using System.Linq;

namespace Pedagogy.Custom.Tasks
{
    public class DeleteEventLogsTask : ITask
    {
        private readonly ClassMembers db = new ClassMembers();

        public string Execute(TaskInfo ti)
        {
            var deleteAfterDate = SettingsKeyInfoProvider.GetIntValue("pedagogyDeleteLogsWhen");
            var today = DateTime.Now;
            var date = today.AddMonths(-deleteAfterDate);

            var eventLogs = db.EventLogs.Where(e => e.EventCreatedOn <= date).ToList();

            foreach (var item in eventLogs)
            {
                db.EventLogs.Remove(item);
                db.SaveChanges();
            }

            return null;
        }
    }
}