﻿using CMS.EventLog;
using CMS.Scheduler;

using System;
using System.Linq;

namespace Pedagogy.Custom.Tasks

{
    public class CreateContactsForFacilities : ITask
    {
        private readonly ClassMembers db = new ClassMembers();
        private readonly ClassBuilder cb = new ClassBuilder();

        public string Execute(TaskInfo task)
        {
            try
            {
                var primaryType = db.ContactTypes.Where(x => x.TypeCodeName.Contains("primaryContact")).Select(x => x.TypeID).FirstOrDefault();
                var alternateType = db.ContactTypes.Where(x => x.TypeCodeName.Contains("alternateContact")).Select(x => x.TypeID).FirstOrDefault();
                var acctsPayableType = db.ContactTypes.Where(x => x.TypeCodeName.Contains("accountsPayableContact")).Select(x => x.TypeID).FirstOrDefault();
                var donType = db.ContactTypes.Where(x => x.TypeCodeName.Contains("directorOfNursingContact")).Select(x => x.TypeID).FirstOrDefault();
                var educatorType = db.ContactTypes.Where(x => x.TypeCodeName.Contains("educatorContact")).Select(x => x.TypeID).FirstOrDefault();

                foreach (var item in db.Facilities.ToList())
                {
                    if (!string.IsNullOrEmpty(item.PrimaryName) || !string.IsNullOrEmpty(item.PrimaryEmail))
                    {
                        var splitName = item.PrimaryName.Split(new char[] { ' ' }, 2);
                        AddNewContact(item.Enabled, item.FacilityID, primaryType, splitName, item.PrimaryEmail, item.PrimaryPhone);
                    }

                    if (!string.IsNullOrEmpty(item.AlternateName) || !string.IsNullOrEmpty(item.AlternateEmail))
                    {
                        var splitName = item.AlternateName.Split(new char[] { ' ' }, 2);
                        AddNewContact(item.Enabled, item.FacilityID, alternateType, splitName, item.AlternateEmail, item.AlternatePhone);
                    }

                    if (!string.IsNullOrEmpty(item.APName) || !string.IsNullOrEmpty(item.APEmail))
                    {
                        var splitName = item.APName.Split(new char[] { ' ' }, 2);
                        AddNewContact(item.Enabled, item.FacilityID, acctsPayableType, splitName, item.APEmail, item.APPhone);
                    }

                    if (!string.IsNullOrEmpty(item.DONName) || !string.IsNullOrEmpty(item.DONEmail))
                    {
                        var splitName = item.DONName.Split(new char[] { ' ' }, 2);
                        AddNewContact(item.Enabled, item.FacilityID, donType, splitName, item.DONEmail, item.DONPhone);
                    }

                    if (!string.IsNullOrEmpty(item.EducatorName) || !string.IsNullOrEmpty(item.EducatorEmail))
                    {
                        var splitName = item.EducatorName.Split(new char[] { ' ' }, 2);
                        AddNewContact(item.Enabled, item.FacilityID, educatorType, splitName, item.EducatorEmail, item.EducatorPhone);
                    }
                }
            }
            catch (Exception ex)
            {
                EventLogProvider.LogException("CreateContactsForFacilities", "ERROR", ex);
            }

            return null;
        }

        private string FormatPhoneNumber(string phoneNum)
        {
            return phoneNum.Replace("-", "").Replace("(", "").Replace(")", "").Replace("cell", "").Trim();
        }

        private void AddNewContact(bool enabled, int facilityID, int typeID, string[] splitName, string email, string phone)
        {
            var contact = db.Contacts
                .Where(x => x.ContactEmail == email)
                .Where(x => x.ContactPhone == phone)
                .Where(x => x.ContactTypeID == typeID);

            var newContact = new Contact() { };

            if (splitName.Length == 1)
            {
                newContact.ContactFirstName = splitName[0];
                contact = contact.Where(x => x.ContactFirstName == newContact.ContactFirstName);
            }
            if (splitName.Length == 2)
            {
                newContact.ContactFirstName = splitName[0];
                newContact.ContactLastName = splitName[1];

                contact = contact.Where(x => x.ContactFirstName == newContact.ContactFirstName);
                contact = contact.Where(x => x.ContactLastName == newContact.ContactLastName);
            }

            if(contact.FirstOrDefault() == null)
            {
                var today = DateTime.Now;
                var defaultVoice = cb.TU_SpeechVoices.Where(x => x.IsDefault.Value).Select(x => x.SpeechVoiceId).FirstOrDefault();
                var defaultRate = 2;

                newContact.ContactEmail = !string.IsNullOrEmpty(email) ? email : null;
                newContact.ContactPhone = !string.IsNullOrEmpty(phone) ? FormatPhoneNumber(phone) : null;
                newContact.CreatedOn = today;
                newContact.SpeechVoiceID = defaultVoice;
                newContact.SpeechRateID = defaultRate;
                newContact.ContactTypeID = typeID;

                db.Contacts.Add(newContact);
                db.SaveChanges();

                var newContactFacility = new ContactFacility
                {
                    ContactID = newContact.ContactID,
                    FacilityID = facilityID,
                    Enabled = enabled
                };

                db.ContactFacilities.Add(newContactFacility);
                db.SaveChanges();
            }
        }
    }
}
