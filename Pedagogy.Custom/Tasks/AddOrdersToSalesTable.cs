﻿using CMS.DataEngine;
using CMS.DocumentEngine.Types.Pedagogy;
using CMS.Ecommerce;
using CMS.Helpers;
using CMS.Localization;
using CMS.Scheduler;
using CMS.SiteProvider;

using Pedagogy.Custom.Generated.ModuleClasses;

using System;
using System.Collections.Generic;
using System.Linq;

namespace Pedagogy.Custom.Tasks
{
    public class AddOrdersToSalesTable : ITask
    {
        private readonly ClassBuilder cb = new ClassBuilder();

        public string Execute(TaskInfo ti)
        {
            decimal transactionPercentFee = SettingsKeyInfoProvider.GetDecimalValue("authorizeNetTransactionFeePercent") / 100;
            decimal transactionCentsFee = SettingsKeyInfoProvider.GetDecimalValue("authorizeNetTransactionFeeCents");
            decimal salesCommissionPercent = SettingsKeyInfoProvider.GetDecimalValue("SalesCommissionPercent") / 100;

            var demoOrders = OrderInfoProvider.GetOrders()
                .OnSite(SiteContext.CurrentSiteName)
                .WhereEquals("OrderIsDemo", true);

            foreach(var o in demoOrders)
            {
                var invoice = int.Parse(o.OrderInvoiceNumber);

                var salesInfo = SalesInfoProvider.GetSales()
                    .Where(x => x.SaleInvoiceNumber == invoice)
                    .ToList();

                if(salesInfo.Count() > 0)
                {
                    foreach (var item in salesInfo)
                    {
                        SalesInfoProvider.DeleteSalesInfo(item);
                    }
                }

                o.SetValue("OrderSalesCommissionProcessed", false);
                o.SetValue("OrderSalesCommissionProcessDate", null);
                o.Update();
            }

            var orders = OrderInfoProvider.GetOrders()
                .OnSite(SiteContext.CurrentSiteName)
                .WhereGreaterOrEquals(nameof(OrderInfo.OrderDate), DateTime.Today.AddMonths(-2))
                .WhereEquals(nameof(OrderInfo.OrderIsPaid), true)
                .WhereEquals("OrderSalesCommissionProcessed", false)
                .WhereEquals("OrderIsDemo", false);

            foreach (var order in orders)
            {
                var items = OrderItemInfoProvider.GetOrderItems(order.OrderID);
                //Set the type
                var type = "";

                //facility order
                if (order.OrderCustomData.ContainsColumn("FacilityID"))
                {
                    //membership purchase
                    if (order.OrderCustomData.ContainsColumn("MembershipPurchase"))
                    {
                        type = ResHelper.GetString("SalesReport.PurchaseType.FacilityMembership", LocalizationContext.CurrentCulture.CultureCode);

                    }
                    //regular class purchase
                    else
                    {
                        type = ResHelper.GetString("SalesReport.PurchaseType.FacilityClass", LocalizationContext.CurrentCulture.CultureCode);

                    }
                }

                var options = items.Where(x => x.OrderItemParentGUID != null).ToList();

                foreach (var item in items)
                {
                    if (!item.OrderItemSKU.IsProductOption)
                    {
                        if (item.OrderItemSKU.SKUProductType == SKUProductTypeEnum.Product && (item.OrderItemBundleGUID == null || item.OrderItemBundleGUID == Guid.Empty))
                        {
                            var classID = int.Parse(item.OrderItemSKU.SKUNumber);
                            var quantity = item.OrderItemUnitCount;

                            var totalPrice = decimal.Round(item.OrderItemBundleGUID == null || item.OrderItemBundleGUID == Guid.Empty ? item.OrderItemTotalPrice : GetTotalItemPrice(item) * quantity, 2);
                            decimal discount = new decimal(0.00);
                            decimal printPrice = new decimal(0.00);

                            if (item.OrderItemBundleGUID != null && item.OrderItemBundleGUID != Guid.Empty)
                            {
                                discount = decimal.Round(totalPrice * new decimal(.1), 2);

                                var price = options.Where(x => x.OrderItemParentGUID == item.OrderItemBundleGUID).Select(x => x.OrderItemUnitPrice).FirstOrDefault();
                                printPrice = decimal.Round(price / items.Where(x => x.OrderItemBundleGUID == item.OrderItemBundleGUID).Count(), 2);

                                if (string.IsNullOrEmpty(type)) type = ResHelper.GetString("SalesReport.PurchaseType.DriveByPackage", LocalizationContext.CurrentCulture.CultureCode);
                            }
                            else
                            {
                                printPrice = options.Where(x => x.OrderItemParentGUID == item.OrderItemGUID).Select(x => x.OrderItemUnitPrice).FirstOrDefault();

                                if (string.IsNullOrEmpty(type)) type = ResHelper.GetString("SalesReport.PurchaseType.DriveByClass", LocalizationContext.CurrentCulture.CultureCode);
                            }

                            var ecomFee = decimal.Round((totalPrice - discount + printPrice) != 0 ? ((totalPrice - discount + printPrice) * transactionPercentFee) + transactionCentsFee : new decimal(0.00), 2);
                            var net = decimal.Round((totalPrice - discount + printPrice) != 0 ? (totalPrice - discount + printPrice) - ecomFee : 0, 2);

                            var newSalesItem = new SalesInfo
                            {
                                SaleClassID = classID,
                                SaleAuthorName = cb.TU_ClassAuthor_XREF.Where(x => x.ClassID == classID).Select(x => x.AuthorName).FirstOrDefault(),
                                SaleDate = order.OrderDate,
                                SaleClassName = item.OrderItemSKU.SKUName,
                                SaleNote = order.OrderNote,
                                SaleInvoiceNumber = int.Parse(order.OrderInvoiceNumber),
                                SaleReconciledOn = DateTime.Now,
                                SaleQuantity = quantity,
                                SaleType = type,
                                SaleCost = totalPrice,
                                SaleDiscount = discount,
                                SaleSubtotal = decimal.Round(totalPrice - discount + printPrice, 2),
                                SaleECommerceFee = ecomFee,
                                SaleNetTotal = net,
                                SaleCommission = decimal.Round(net * salesCommissionPercent, 2),
                                SaleAuthorCommission = decimal.Round((net - (net * salesCommissionPercent)) / 2, 2)
                            };

                            SalesInfoProvider.SetSalesInfo(newSalesItem);
                        }
                        else if (item.OrderItemSKU.SKUProductType == SKUProductTypeEnum.Bundle)
                        {
                            if (string.IsNullOrEmpty(type))
                            {
                                CourseMembership courseMem = CourseMembershipProvider.GetCourseMemberships().WhereEquals(nameof(CourseMembership.SKU.SKUID), item.OrderItemSKUID);

                                if (courseMem != null) type = ResHelper.GetString("SalesReport.PurchaseType.DriveByMembership", LocalizationContext.CurrentCulture.CultureCode);
                            }

                            if (string.IsNullOrEmpty(type)) type = ResHelper.GetString("SalesReport.PurchaseType.DriveByPackage", LocalizationContext.CurrentCulture.CultureCode);

                            var bundleItems = SKUInfoProvider.GetBundleItems(item.OrderItemSKUID);

                            foreach (var i in bundleItems)
                            {
                                if (!i.IsProductOption)
                                {
                                    var quantity = item.OrderItemUnitCount;

                                    var totalPrice = decimal.Round(i.SKUPrice * quantity, 2);
                                    decimal discount = decimal.Round(totalPrice * new decimal(.1), 2);

                                    var price = options.Where(x => x.OrderItemParentGUID == item.OrderItemBundleGUID).Select(x => x.OrderItemUnitPrice).FirstOrDefault();
                                    decimal printPrice = decimal.Round(price / bundleItems.Count, 2);

                                    var ecomFee = decimal.Round((totalPrice - discount + printPrice) != 0 ? ((totalPrice - discount + printPrice) * transactionPercentFee) + transactionCentsFee : new decimal(0.00), 2);
                                    var net = decimal.Round((totalPrice - discount + printPrice) != 0 ? (totalPrice - discount + printPrice) - ecomFee : 0, 2);

                                    var classID = int.Parse(i.SKUNumber);
                                    var newSalesItem = new SalesInfo
                                    {
                                        SaleClassID = classID,
                                        SaleAuthorName = cb.TU_ClassAuthor_XREF.Where(x => x.ClassID == classID).Select(x => x.AuthorName).FirstOrDefault(),
                                        SaleQuantity = quantity,
                                        SaleClassName = i.SKUName,
                                        SaleDate = order.OrderDate,
                                        SaleType = type,
                                        SaleInvoiceNumber = int.Parse(order.OrderInvoiceNumber),
                                        SaleNote = order.OrderNote,
                                        SaleCost = totalPrice,
                                        SaleDiscount = discount,
                                        SaleSubtotal = decimal.Round(totalPrice - discount + printPrice, 2),
                                        SaleECommerceFee = ecomFee,
                                        SaleNetTotal = net,
                                        SaleCommission = decimal.Round(net * salesCommissionPercent, 2),
                                        SaleAuthorCommission = decimal.Round((net - (net * salesCommissionPercent)) / 2, 2),
                                        SaleReconciledOn = DateTime.Now
                                    };

                                    SalesInfoProvider.SetSalesInfo(newSalesItem);
                                }
                            }
                        }
                    }
                }

                order.SetValue("OrderSalesCommissionProcessed", true);
                order.SetValue("OrderSalesCommissionProcessDate", DateTime.Now);
                order.Update();
            }

            return null;
        }

        private decimal GetTotalItemPrice(OrderItemInfo item)
        {
            if (item.OrderItemBundleGUID != null && item.OrderItemBundleGUID != Guid.Empty)
            {
                return SKUInfoProvider.GetSKUInfo(item.OrderItemSKUID).SKUPrice;
            }

            return item.OrderItemTotalPrice;
        }

        private int GetBundleParentItemQuantity(OrderItemInfo child, IEnumerable<OrderItemInfo> items)
        {
            return items.Where(x => x.OrderItemGUID == child.OrderItemParentGUID).Select(x => x.OrderItemUnitCount).FirstOrDefault();
        }
    }
}
