﻿using CMS.Scheduler;
using System.Linq;

namespace Pedagogy.Custom.Tasks
{
    public class SplitContactFirstNameTask : ITask
    {
        private readonly ClassMembers db = new ClassMembers();

        public string Execute(TaskInfo task)
        {
            var contacts = db.Contacts
                .Where(x => string.IsNullOrEmpty(x.ContactLastName))
                .Select(x => new Contact { ContactFirstName = x.ContactFirstName, ContactLastName = x.ContactLastName })
                .ToList();

            foreach(var contact in contacts)
            {
                if(contact.ContactFirstName.Split(' ').Count() > 0)
                {
                    var name = contact.ContactFirstName.Split(new char[] { ' ' }, 2);

                    contact.ContactFirstName = name[0];
                    if(name.Count() > 1)
                    {
                        contact.ContactLastName = name[1];
                    }

                    db.Entry(contact).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }

            return null;
        }
    }
}
