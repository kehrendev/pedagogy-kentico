﻿using CMS.Scheduler;
using System;
using System.Linq;

namespace Pedagogy.Custom.Tasks
{
    public class DeleteExpiredMembershipsTask : ITask
    {
        private readonly ClassMembers db = new ClassMembers();

        public string Execute(TaskInfo ti)
        {
            var today = DateTime.Now;
            var mems = db.FacilityMemberships.ToList();
            var startWF = db.ClassWorkflows.Where(w => w.WorkflowCodeName.Contains("startClass")).Select(w => w.ClassWorkflowID).FirstOrDefault();

            if (mems.Count > 0)
            {
                foreach (var item in mems)
                {
                    if (item.ExpirationDate <= today)
                    {
                        var classes = db.FacilityClasses
                            .Where(f => f.FacilityID == item.FacilityID)
                            .Where(f => f.FacilityMembershipID == item.FacilityMembershipID)
                            .ToList();

                        foreach (var i in classes)
                        {
                            var studentClass = db.StudentClasses.Where(sc => sc.ClassID == i.ClassID).Where(sc => sc.FacilityMembershipID == i.FacilityMembershipID).FirstOrDefault();

                            if (studentClass != null && studentClass.ClassWorkflowID == startWF)
                            {
                                db.StudentClasses.Remove(studentClass);
                                db.SaveChanges();
                            }

                            db.FacilityClasses.Remove(i);
                            db.SaveChanges();
                        }

                        db.FacilityMemberships.Remove(item);
                        db.SaveChanges();
                    }
                }
            }            

            var studentClasses = db.StudentClasses.Where(sc => sc.ExpiresOn.HasValue).ToList();

            if(studentClasses.Count > 0)
            {
                foreach (var item in studentClasses)
                {
                    if (item.ExpiresOn.Value <= today && item.ClassWorkflowID == startWF)
                    {
                        var studentMem = db.StudentMemberships.Where(sm => sm.StudentMembershipID == item.StudentMembershipID).FirstOrDefault();
                        db.StudentMemberships.Remove(studentMem);
                        db.StudentClasses.Remove(item);
                        db.SaveChanges();
                    }
                }
            }
            
            return null;
        }
    }
}
