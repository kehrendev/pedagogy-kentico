﻿using CMS.Membership;
using CMS.Scheduler;

using System;
using System.Linq;

namespace Pedagogy.Custom.Tasks
{
    public class CreateAdminContacts : ITask
    {
        public string Execute(TaskInfo task)
        {
            using (var db = new ClassMembers())
            {
                var corpAdminType = db.ContactTypes.Where(x => x.TypeCodeName == "corporateAdmin").Select(x => x.TypeID).FirstOrDefault();
                var corpAdmins = db.ContactFacilities.Where(x => x.IsCorpAdmin.HasValue).Where(x => !x.ContactID.HasValue).ToList();

                foreach(var corpAdmin in corpAdmins)
                {
                    if (corpAdmin.OldContactID.HasValue)
                    {
                        var user = UserInfoProvider.GetUserInfo(corpAdmin.OldContactID.Value);

                        if(user != null)
                        {
                            var newContact = new Contact
                            {
                                ContactUserID = user.UserID,
                                ContactFirstName = user.FirstName,
                                ContactLastName = user.LastName,
                                ContactEmail = user.Email,
                                ContactPhone = user.UserSettings != null ? user.UserSettings.UserPhone : "",
                                CreatedOn = DateTime.Now,
                                SpeechVoiceID = 2, // default speech voice
                                SpeechRateID = 2, // default speech rate
                                ContactTypeID = corpAdminType
                            };

                            db.Contacts.Add(newContact);
                            db.Entry(newContact).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();

                            corpAdmin.ContactID = newContact.ContactID;
                            db.Entry(corpAdmin).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                }

                var facilityAdminType = db.ContactTypes.Where(x => x.TypeCodeName == "facilityAdmin").Select(x => x.TypeID).FirstOrDefault();
                var facilityAdmins = db.ContactFacilities.Where(x => x.IsFacilityAdmin.HasValue).Where(x => !x.ContactID.HasValue).ToList();

                foreach(var facilityAdmin in facilityAdmins)
                {
                    if (facilityAdmin.OldContactID.HasValue)
                    {
                        var user = UserInfoProvider.GetUserInfo(facilityAdmin.OldContactID.Value);

                        if(user != null)
                        {
                            var newContact = new Contact
                            {
                                ContactUserID = user.UserID,
                                ContactFirstName = user.FirstName,
                                ContactLastName = user.LastName,
                                ContactEmail = user.Email,
                                ContactPhone = user.UserSettings != null ? user.UserSettings.UserPhone : "",
                                CreatedOn = DateTime.Now,
                                SpeechVoiceID = 2, // default speech voice
                                SpeechRateID = 2, // default speech rate
                                ContactTypeID = facilityAdminType
                            };

                            db.Contacts.Add(newContact);
                            db.Entry(newContact).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();

                            facilityAdmin.ContactID = newContact.ContactID;
                            db.Entry(facilityAdmin).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                }
            };

            return null;
        }
    }
}
