﻿using CMS;
using CMS.Base;
using CMS.DataEngine;
using CMS.IO;
using CMS.Helpers;
using CMS.SiteProvider;

// Registers the custom module into the system
[assembly: RegisterModule(typeof(Pedagogy.Custom.Storage.CustomAzureStorageModule))]

namespace Pedagogy.Custom.Storage
{
    public class CustomAzureStorageModule : Module
    {
        public CustomAzureStorageModule()
            : base("CustomAzureStorageModule") { }

        protected override void OnInit()
        {
            base.OnInit();
            var sites = SiteInfoProvider.GetSites().WhereEquals("SiteIsOffline", 0).And().WhereEquals("SiteStatus", "RUNNING");
            // loop through all sites in this instance and create a new provider for each one
            foreach (SiteInfo site in sites)
            {
                // Creates a new StorageProvider instance for media files
                AbstractStorageProvider mediaProvider = CustomAzureStorageModule.CreateAzureStorageProvider();

                // Maps a directory to the provider
                string path = string.Format("/{0}/media", site.SiteName);
                StorageHelper.MapStoragePath("~" + path, mediaProvider);

                // Creates a new StorageProvider instance for files
                AbstractStorageProvider fileProvider = CustomAzureStorageModule.CreateAzureStorageProvider();

                string filePath = string.Format("/{0}/files", site.SiteName);
                StorageHelper.MapStoragePath("~" + filePath, fileProvider);
            }

        }

        public static AbstractStorageProvider CreateAzureStorageProvider()
        {
            // Creates a new StorageProvider instance
            AbstractStorageProvider azureStorageProvider = new StorageProvider("Azure", "CMS.AzureStorage");

            // Specifies the target container
            azureStorageProvider.CustomRootPath = ValidationHelper.GetString(SettingsHelper.AppSettings["CMSAzureRootContainer"], "");

            // Makes the container publicly accessible
            azureStorageProvider.PublicExternalFolderObject = ValidationHelper.GetBoolean(SettingsHelper.AppSettings["AzureIsStoragePublic"], false);

            return azureStorageProvider;
        }
    }
}