using System;
using System.Data;

using CMS.Base;
using CMS.DataEngine;
using CMS.Helpers;

namespace Pedagogy
{
    /// <summary>
    /// Class providing <see cref="SalesInfo"/> management.
    /// </summary>
    public partial class SalesInfoProvider : AbstractInfoProvider<SalesInfo, SalesInfoProvider>
    {
        /// <summary>
        /// Creates an instance of <see cref="SalesInfoProvider"/>.
        /// </summary>
        public SalesInfoProvider()
            : base(SalesInfo.TYPEINFO)
        {
        }


        /// <summary>
        /// Returns a query for all the <see cref="SalesInfo"/> objects.
        /// </summary>
        public static ObjectQuery<SalesInfo> GetSales()
        {
            return ProviderObject.GetObjectQuery();
        }


        /// <summary>
        /// Returns <see cref="SalesInfo"/> with specified ID.
        /// </summary>
        /// <param name="id"><see cref="SalesInfo"/> ID.</param>
        public static SalesInfo GetSalesInfo(int id)
        {
            return ProviderObject.GetInfoById(id);
        }


        /// <summary>
        /// Returns <see cref="SalesInfo"/> with specified name.
        /// </summary>
        /// <param name="name"><see cref="SalesInfo"/> name.</param>
        public static SalesInfo GetSalesInfo(string name)
        {
            return ProviderObject.GetInfoByCodeName(name);
        }


        /// <summary>
        /// Sets (updates or inserts) specified <see cref="SalesInfo"/>.
        /// </summary>
        /// <param name="infoObj"><see cref="SalesInfo"/> to be set.</param>
        public static void SetSalesInfo(SalesInfo infoObj)
        {
            ProviderObject.SetInfo(infoObj);
        }


        /// <summary>
        /// Deletes specified <see cref="SalesInfo"/>.
        /// </summary>
        /// <param name="infoObj"><see cref="SalesInfo"/> to be deleted.</param>
        public static void DeleteSalesInfo(SalesInfo infoObj)
        {
            ProviderObject.DeleteInfo(infoObj);
        }


        /// <summary>
        /// Deletes <see cref="SalesInfo"/> with specified ID.
        /// </summary>
        /// <param name="id"><see cref="SalesInfo"/> ID.</param>
        public static void DeleteSalesInfo(int id)
        {
            SalesInfo infoObj = GetSalesInfo(id);
            DeleteSalesInfo(infoObj);
        }
    }
}