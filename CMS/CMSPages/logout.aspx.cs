﻿using System;
using System.Configuration;
using System.Data;
using System.Web;
using System.Text;
using System.Web.UI;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using CMS.Base;
using CMS.Base.Web.UI;
using CMS.Core;
using CMS.DataEngine;
using CMS.EventLog;
using CMS.Helpers;
using CMS.Localization;
using CMS.Membership;
using CMS.MembershipProvider;
using CMS.PortalEngine.Web.UI;
using CMS.SiteProvider;
using CMS.UIControls;
using CMS.ExternalAuthentication;
using CMS.PortalEngine;
using CMS.DocumentEngine;
using CMS.MacroEngine;

public partial class CMSPages_logout : CMSPage
{

    protected void Page_Load(object sender, EventArgs e)
    {
        string redirectUrl = "/";
        if (AuthenticationHelper.IsAuthenticated())
        {
            // If the user has registered Windows Live ID
            if (!String.IsNullOrEmpty(MembershipContext.AuthenticatedUser.UserSettings.WindowsLiveID))
            {
                // Get data from auth cookie
                string[] userData = AuthenticationHelper.GetUserDataFromAuthCookie();
            }

            PortalContext.ViewMode = ViewModeEnum.LiveSite;
            AuthenticationHelper.SignOut();

            Response.Cache.SetNoStore();
            URLHelper.Redirect(UrlResolver.ResolveUrl(redirectUrl));
        }
        else
        {
            URLHelper.Redirect(UrlResolver.ResolveUrl(redirectUrl));
        }
    }
}
